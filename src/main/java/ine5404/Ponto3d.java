package ine5404;

public class Ponto3d extends  Ponto2d {
    protected int z;

    public Ponto3d(int x, int y, int z){
    	super(x,y);
    	this.z = z;
    }

    //faltam getters and setters

    public String toString() {
    	return super.toString() + ", " + this.z;
    }
}
