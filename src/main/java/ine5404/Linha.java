package ine5404;

public class Linha {
    protected Ponto2d p1, p2;

    public Linha(Ponto2d p1, Ponto2d p2){
    	this.p1 = p1;
    	this.p2 = p2;
    }

    //faltam getters and setters

    public String toString(){
    	return "Linha: " + this.p1.toString() + ", " + this.p2.toString();
    }
}
